#pragma once

#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>
#include "sqlite3.h"

using namespace std;

typedef unsigned int Uint;

int callback(void* notUsed, int argc, char** argv, char** azCol);
int getLastId(void* notUsed, int argc, char** argv, char** azCol);