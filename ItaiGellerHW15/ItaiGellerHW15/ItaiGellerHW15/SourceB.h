#include "stdafx.h"
#include <sstream>

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg); //Tries to purchase a car. Returns true in success else false
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg); //Safely transfers money from one account to another
int getInt(void* notUsed, int argc, char** argv, char** azCol); //A callback used to get ints out of the db
void q2(); //q2 Solution